# Wrapper framework for debugging proof-of-concept exploits

## Details
See the classroom exercise on Canvas for instructions

## Reference
* JHU-ISI, Software Vulnerability Analysis, EN.650.660
* Reuben Johnston, reub@jhu.edu
